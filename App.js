import { useEffect, useState } from "react";
import Header from "./components/Header";
import { QuestionField } from "./components/QuestionField";
import Footer from "./footer";
import { FetchQuestion } from "./components/QuestionField";

function App() {
  // useEffect(() => {
  //   FetchQuestion();
  // }, []);

  return (
    <div>
      <Header />
      <QuestionField />
      <Footer />
    </div>
  );
}
export default App;
